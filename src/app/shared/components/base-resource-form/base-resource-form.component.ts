import {AfterContentChecked, Directive, Injector, OnInit} from '@angular/core';
import {FormBuilder, FormGroup,} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

import {BaseResourceModel} from "../../models/base-resource.model";
import {BaseResourceService} from "../../services/base-resource.service";

import * as toastr from 'toastr';

export default toastr;

@Directive()
export abstract class BaseResourceFormComponent<T extends BaseResourceModel> implements OnInit, AfterContentChecked {
  currencyAction: string; // editando ou criando novo recurso

  resourceForm: FormGroup;
  pageTitle: string;
  serverErrorMessages: string[] = null;
  submittingForm = false;

  protected route: ActivatedRoute;
  protected router: Router;
  protected formBuilder: FormBuilder;

  constructor(
    protected injector: Injector,
    public resource: T,
    protected resourceService: BaseResourceService<T>,
    protected jsonDataToResourceFn: (jsonData) => T
  ) {
    this.route = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.formBuilder = injector.get(FormBuilder);

  }

  ngOnInit(): void {
    this.setCurrencyAction();
    this.buildResourceForm();
    this.loadResource();
  }

  protected abstract buildResourceForm(): void;

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  submitForm(): void {
    this.submittingForm = true;

    if (this.currencyAction === 'new') {
      this.createResource();
    } else {
      this.updateResource();
    }
  }

  protected setPageTitle(): void {
    if (this.currencyAction === 'new')
      this.pageTitle = this.creationPageTitle();
    else
      this.pageTitle = this.editionPageTitle();

  }

  protected creationPageTitle(): string {
    return "Novo";
  }

  protected editionPageTitle(): string {
    return "Edição";
  }

  protected setCurrencyAction(): void {
    this.currencyAction =
      this.route.snapshot.url[0].path === 'new' ? 'new' : 'edit';
  }

  protected loadResource(): void {
    if (this.currencyAction === 'edit') {
      this.route.paramMap
        .pipe(
          switchMap((params) => this.resourceService.getById(+params.get('id')))
        )
        .subscribe(
          (resource) => {
            this.resource = resource;
            this.resourceForm.patchValue(this.resource); // binds loaded resource data to ResourceForm
          },
          (error) => alert('Ocorreu um erro no servidor')
        );
    }
  }

  protected createResource(): void {
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);
    this.resourceService.create(resource).subscribe(
      (resourceTemp) => this.actionsForSuccess(resourceTemp),
      (error) => this.actionsForError(error)
    );
  }

  protected updateResource(): void {
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.resourceService.update(resource).subscribe(
      (resourceTemp) => this.actionsForSuccess(resourceTemp),
      (error) => this.actionsForError(error)
    );
  }

  protected actionsForSuccess(resource: T): void {
    toastr.success('Solicitação processada com sucesso!');
    const baseComponentPath = this.route.snapshot.parent.url[0].path;

    // redirect/reload component page
    this.router
      .navigateByUrl(baseComponentPath, {skipLocationChange: true})
      .then(() => this.router.navigate([baseComponentPath, resource.id, 'edit']));
  }

  protected actionsForError(error): void {
    toastr.error('Falha ao processar sua solicitação!');

    this.submittingForm = false;

    // Bad request 400 tbm pode ser utilizado
    // adequar de acordo com o back end que for utilizado
    if (error.status === 422)
      this.serverErrorMessages = JSON.parse(error._body);
    else
      this.serverErrorMessages = [
        'Falha na comunicação com o servidor. Tente novamente mais tarde!',
      ];

  }
}
